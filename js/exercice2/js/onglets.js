var onglet = document.querySelectorAll('.onglet a');

for (var i = 0; i < onglet.length; i++){
    onglet[i].addEventListener('click', function (e){

        var li = this.parentNode;
        var div = this.parentNode.parentNode.parentNode;

        if(li.classList.contains('active')){
            return false;
        }
        //remove la class de l'onglet actif
        div.querySelector('.onglet .active').classList.remove('active');
        //ajout de la class a l'onglet actif
        li.classList.add('active');
        //on retire la classe active sur le contenu
        div.querySelector('.onglet-content .active').classList.remove('active');
        //ajout de la classe active sur le contenu correspondant au click
        var elem =(this.getAttribute('href'));
        //div.querySelector(elem).classList.add('active');
        document.getElementById(elem).classList.add('active');
    })
}