var character = {
    knight :{
        hp : 0,
        dmg : 0,
        turn : false,
        hitChance : 0,
    },
    dragon : {
        hp : 0,
        dmg : 0,
        turn : false,
        hitChance : 0,
    },
};

var knight = prompt('quel est le niveau du chevalier? weak, medium ou strong?');
var dragon = prompt('quel est le niveau du dragon? weak, medium ou strong?');
var maxTurn = 10;
var hitChance = Math.floor(Math.random() * (1 - 0 +1)) + 0;

switch (knight){
    case 'weak' : 
    character.knight.hp = 100;
    character.knight.dmg = Math.random() * (20 - 10) + 10;
    character.knight.turn = true;
    break;
    case 'medium' :
    character.knight.hp = 150;
    character.knight.dmg = Math.random() * (30 - 15) + 15;
    character.knight.turn = true;
    break;
    case 'strong' :
    character.knight.hp = 200;
    character.knight.dmg = Math.random() * (40 - 20) + 20;
    character.knight.turn = true;
}

switch (dragon){
    case 'weak' : 
    character.dragon.hp = 150;
    character.dragon.dmg = Math.random() * (15 - 10) + 10;
    character.dragon.turn = true;
    break;
    case 'medium' :
    character.dragon.hp = 200;
    character.dragon.dmg = Math.random() * (20 - 15) + 15;
    character.dragon.turn = true;
    break;
    case 'strong' :
    character.dragon.hp = 250;
    character.dragon.dmg = Math.random() * (30 - 20) + 20;
    character.dragon.turn = true;
}

switch (hitChance){
    case '0' :
    break;
    case '1' :
    break;
}