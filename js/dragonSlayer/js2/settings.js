function character(hp, dmg, turn){
    this.hp = hp;
    this.dmg = dmg;
    this.turn = turn;
}

var player = prompt('quel est le niveau de votre perso? weak, medium or strong?');

var computer = prompt('quel est le niveau du dragon? weak, medium or strong?')

switch (player){
    case 'weak' : 
    knightHp = 100;
    knightDmg = Math.floor(Math.random() * (20 - 10 +1)) + 10;
    knightTurn = true;
    break;
    case 'medium' :
    knightHp = 150;
    knightDmg = Math.floor(Math.random() * (30 - 15 +1)) + 15;
    knightTurn = true;
    break;
    case 'strong' :
    knightHp = 200;
    knightDmg = Math.floor(Math.random() * (40 - 20 +1)) + 20;
    knightTurn = true;
}

switch (computer){
    case 'weak' : 
    dragonHp = 150;
    dragonDmg = Math.random() * (15 - 10) + 10;
    dragonTurn = true;
    break;
    case 'medium' :
    dragonHp = 200;
    dragonDmg = Math.random() * (20 - 15) + 15;
    dragonTurn = true;
    break;
    case 'strong' :
    dragonHp = 250;
    dragonDmg = Math.random() * (30 - 20) + 20;
    dragonTurn = true;
}

var knight = new character(knightHp, knightDmg, knightTurn);

var dragon = new character(dragonHp, dragonDmg, dragonTurn);
